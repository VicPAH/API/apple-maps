resource "random_id" "code_bucket" {
  byte_length = 2
  prefix      = "function_code_"
}
resource "google_storage_bucket" "code" {
  name = random_id.code_bucket.hex
  # project = data.google_client_config.current.project
}
resource "google_storage_bucket_object" "code" {
  name         = "package.zip"
  bucket       = google_storage_bucket.code.name
  source       = "../package.zip"
  content_type = "application/zip"
  metadata     = {}
}
resource "google_cloudfunctions_function" "create_token" {
  name        = "create_token"
  description = "Create token"
  runtime     = "nodejs12"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.code.name
  source_archive_object = google_storage_bucket_object.code.name
  trigger_http          = true
  entry_point           = "createToken"

  environment_variables = {
    apple_maps_key_b64 = var.apple_maps_key_b64
  }

  depends_on = [google_project_service.cloudfunctions]
}
resource "google_cloudfunctions_function_iam_binding" "public" {
  cloud_function = google_cloudfunctions_function.create_token.id
  role           = "roles/cloudfunctions.invoker"
  members        = ["allUsers"]
}
