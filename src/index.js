const jwt = require('jsonwebtoken');
const process = require('process');


const JWK_PEM = Buffer.from(process.env.apple_maps_key_b64, 'base64').toString()
const KEY_ID = '5ZS49YT9Y9'
const TEAM_ID = 'NJ9D5Y2P6P'

const ALLOWED_ORIGINS = [
  'http://localhost:3000',
  'http://192.168.178.32:3000',
  'https://vicpah.gitlab.io',
  'https://vicpah.com.au',
  'https://www.vicpah.com.au',
  'https://vicpah.org.au',
  'https://www.vicpah.org.au',
]


exports.createToken = (req, res) => {
  const origin = req.get('origin')
  if (!origin) {
    return res.status(400).json({ error: 'No origin provided' });
  }
  if (ALLOWED_ORIGINS.indexOf(origin.toLowerCase()) === -1) {
    return res.status(400).json({ error: 'Not an allowed origin' });
  }

  res.set({
    'Access-Control-Allow-Origin': origin,
  });
  return res.status(200).json({
    token: jwt.sign(
      { origin },
      JWK_PEM,
      {
        algorithm: 'ES256',
        keyid: KEY_ID,
        issuer: TEAM_ID,
        expiresIn: '6h',
      },
    ),
  });
};
